# Fork this project
1. Fork this project to your personal gitlab account
1. Invite `henry@artistsworld.me`, `vic@artistsworld.me` as members of your forked project
1. Clone your forked project to your personal computer

Assume that you place this project at `D:\web` (You can put it at any directory of your choice)

# Development Preparation
1. Install NPM
1. Install Visual Studio Code (VSC)
1. Open VSC then `File > Open Folder` and browse to `D:\web\frontend`
1. Open Terminal (inside VSC) (Ctrl + `)
1. Install Vue CLI, run `npm install -g @vue/cli`
1. run `npm install`
1. run `npm run watch`

Open browser and browse to `http://127.0.0.1:8080` and confirm that the site is running properly.

# Development Notes
These below commands are run within the VSC Terminal (Ctrl + ` to open it).

And the cursor should be at `D:\web\frontend>`.

Please open it before running them.

* Start developing daily, run `npm run watch`
* To add vue module run `vue add <module-name>` (ex: `vue add vuex`)
* more later

# [Requirements](https://gitlab.com/aw-recruits/frontend/-/requirements_management/requirements?page=1&state=opened&sort=created_asc)
